# QA engineering task #

This is a simple task for you to showcase a few basic testing automation techniques and good practices. 
The .sln is the default template of a new hosted Blazor WASM SPA. To get started, fork the main branch. After you're done, send us the branch with your code or the zip with the completed task.

### Explanation of the solution ###
* Build by typing `dotnet build` in the root folder (where the `.sln` file is). Sometimes it is good to run `dotnet clean` before the build if the app misbehaves and yo do not have time to debug the exact issue.
* You can run it by typing `dotnet run` if you are in the `Server` folder.
* Make sure you run the app from the `Server` folder and not the `Client`. 
* The counter page is a simple addition machine. The fetch data page calls an API that returns some random data.

### Requirements ###

* The challenge is not to find any bugs. It is about setting up a proper test automation framework. 
* Create 3 testing projects: one for component testing, one for API integration tests and one of UI testing.
	* Component testing can use [bUnit](https://bunit.dev/). It is simple to learn but do not worry about being great at it, we do not judge based on that. 
	* The API integration tests can use whatever framework you want. Examples are HttpCLients in your favorite programming language or API testing tools like Postman.
	* UI testing can use whatever framework you want. Examples are Selenium, Cypress or Playwright.
* For each one setup test all the test cases that would fall under that type of testing. We are looking for a combination of tests from multiple test types that would give us the most confidence in a release candidate.
* We are looking for readability and extensibility before all else. These testing frameworks will be used mainly by FE and BE devs. 
* Do not feel like you have to implement all scenarios if you run out of time or if there are too many. A couple of examples from each test type will do. 

### Notes ###

* There is no one right solution to this, we will use your solution to have a longer chat duting the interview. 
* This is a _very_ simple sample app. Feel free to think ahead and maybe overengineer a bit, add some code and features 
* There is no time limit to this. Take as much time as needed. 
* Bonus points if you create a sample CI file of your preference that runs all those tests at the correct stage. 
* Bonus points if you can add or mention any other type of test project that will complete the test coverage.
* Whatever you don't have time to implement, please make sure you mention in your README as future work.

